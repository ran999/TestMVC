﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Objects
{
    public class ObjUser
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Objects
{
    public class ObjUserList
    {
        public int count { get; set; }

        public List<ObjUserListItem> item { get; set; }
    }
    public class ObjUserListItem
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}

﻿using Core.DB;
using Core.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Service
{
    public class UserService
    {
        DBMVC database;
        public UserService(DBMVC context)
        {
            database = context;
        }
        public void Create(ObjUser obj)
        {
            TUser model = new TUser();
            model.Surname = obj.Surname;
            model.Name = obj.Name;
            model.Phone = obj.Phone;
            model.Email = obj.Email;
            using (var db = database)
            {
                //делаем запрос к БД
                db.TUsers.Add(model);
                db.SaveChanges();
            }
        }
        public void Edit(ObjUser obj)
        {
            using (var db = database)
            {
                //делаем запрос к БД
                var model = db.TUsers.Find(obj.Id);
                model.Surname = obj.Surname;
                model.Name = obj.Name;
                model.Phone = obj.Phone;
                model.Email = obj.Email;

                db.SaveChanges();
            }
        }
        public ObjUserList GetUserList()
        {
            ObjUserList model = new ObjUserList();
            model.item = new List<ObjUserListItem>();
            using (var db = database)
            {
                var list = db.TUsers;
                model.count = list.Count();
                model.item = list.OrderBy(u => u.Id).Select(item => new ObjUserListItem()
                {
                    Id = item.Id,
                    Surname = item.Surname,
                    Name = item.Name,
                    Phone = item.Phone,
                    Email = item.Email
                }).ToList();
            }
            return model;
        }
        public ObjUser GetUsereById(int id)
        {
            if (id == 0)
            {
                return null;
            }
            ObjUser model = null;
            using (var db = database)
            {
                var Item = db.TUsers.Find(id);
                model = Map(Item);
            }
            return model;
        }
        public ObjUser Map(TUser obj)
        {
            if (obj == null)
            {
                return null;
            }
            var model = new ObjUser()
            {
                Id = obj.Id,
                Surname = obj.Surname,
                Name = obj.Name,
                Phone = obj.Phone,
                Email = obj.Email
            };
            return model;
        }
    }
}

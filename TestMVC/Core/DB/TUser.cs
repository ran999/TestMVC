﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DB
{
    public class TUser
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}

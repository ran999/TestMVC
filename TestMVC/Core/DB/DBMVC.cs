﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Core.DB
{
    public class DBMVC:DbContext
    {
        public DbSet<TUser> TUsers { get; set; }

        public DBMVC(DbContextOptions<DBMVC> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}

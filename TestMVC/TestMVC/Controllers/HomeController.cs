﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Core.Objects;
using Core.Service;
using Microsoft.AspNetCore.Mvc;
using TestMVC.Models;

namespace TestMVC.Controllers
{
    public class HomeController : Controller
    {
        Core.DB.DBMVC db;
        public HomeController (Core.DB.DBMVC context)
        {
            db = context;
        }
        public ActionResult Index()
        {
            UserService us = new UserService(db);
            var list = us.GetUserList();
            return View(list);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ObjUser model = new ObjUser();
            return View("Create", model);
        }
        [HttpPost]
        public ActionResult Create(ObjUser model)
        {
            UserService us = new UserService(db);
            us.Create(model);
            
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            UserService us = new UserService(db);
            ObjUser obj = us.GetUsereById(id);
            return View("Edit", obj);
        }
        [HttpPost]
        public ActionResult Edit(ObjUser model)
        {
            UserService us = new UserService(db);
            us.Edit(model as ObjUser);

            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
